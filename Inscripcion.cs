using System;
using System.Collections.Generic;

public class Inscripcion {
    public DateTime Fecha { get; set; }
    public Estudiante alumno { get; set; }
    public Curso en { get; set; }
    public Inscripcion (Estudiante alumno, Curso en) {
        this.alumno = alumno;
        this.en = en;
        this.Fecha = DateTime.Now;
        this.alumno.Registro.Add (this);
        this.en.de.Add (this);
    }
}