using System;

public class Examen {
    private static int _idCount = 1;
    public int Id { get; set; }
    public long LegajoEstudiante { get; set; }
    public int CursoId { get; set; }
    public DateTime Fecha { get; set; }
    public decimal Nota { get; set; }

    public Examen (long LegajoEstudiante, int CursoId, DateTime Fecha, decimal Nota){
        this.Id = _idCount++;
        this.LegajoEstudiante = LegajoEstudiante;
        this.CursoId = CursoId;
        this.Fecha = Fecha;
        this.Nota = Nota;
    }

    public Examen (int CursoId){
        this.Id = _idCount++;
        this.CursoId = CursoId;
    }

    public void CalificarExamen(decimal nota, long legajo, DateTime fecha){
        this.LegajoEstudiante = legajo;
        this.Fecha = fecha;
        this.Nota = nota;
    }
}