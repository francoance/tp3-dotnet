using System;
using System.Collections.Generic;

public class Profesor : Persona {
    public string Titulo { get; set; }
    public List<Curso> Tema { get; set; }

    public Profesor (string nombre, long dni, string titulo) : base (nombre, dni) {
        this.Titulo = titulo;
        this.Tema = new List<Curso> (capacity: 3);
    }
}