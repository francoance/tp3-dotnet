using System;
using System.Collections.Generic;

public class Estudiante : Persona {

    public long Legajo { get; set; }
    public int CantCursos { get; set; }
    public List<Inscripcion> Registro;

    public Estudiante (string nombre, long dni, long legajo) : base (nombre, dni) {
        this.Registro = new List<Inscripcion> ();
        this.Legajo = legajo;
        this.CantCursos = 0;
    }

    public void Inscribir (Curso curso) {
        Inscripcion insc = new Inscripcion (alumno: this, en: curso);
        this.CantCursos++;
        Console.WriteLine ($"{this.Nombre} inscripto en {curso.Nombre}");
    }
}