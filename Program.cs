﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace TP3 {
    class Program {
        static void Main (string[] args) {
            var estudiantes = Repositorio.GetEstudiantes ();
            var profesores = Repositorio.GetProfesores ();
            var cursos = Repositorio.GetCursos ();

            var cursonet = cursos.First (curso => curso.Nombre == "NET");
            var prof = profesores.First (profesor => profesor.Nombre == "Romina");
            var est = estudiantes.First (estudiante => estudiante.Nombre == "Jose");

            cursonet.Instructor = prof;
            est.Inscribir (cursonet);

            var examen = new Examen(cursonet.Id);
            examen.CalificarExamen(6.5m, est.Legajo, DateTime.Now);
            var examenes = new List<Examen>();
            examenes.Add(examen);
            Repositorio.ResultadosExt(examenes, cursos);

            //Console.ReadLine ();
        }
    }
}