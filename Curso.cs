using System;
using System.Collections.Generic;
using System.Linq;

public class Curso {
    private static int _idCount = 1;
    public int Id { get; set; }
    public string Nombre { get; set; }
    public int Duracion { get; set; }
    public List<Inscripcion> de { get; set; }

    private Profesor _Instructor { get; set; }
    public Profesor Instructor {
        get {
            return _Instructor;
        }
        set {
            _Instructor = value;
            _Instructor.Tema.Add (this);
        }
    }

    public Curso (string Nombre, int Duracion) {
        this.Id = _idCount++;
        this.Nombre = Nombre;
        this.Duracion = Duracion;
        de = new List<Inscripcion> (capacity: 50);
    }
    public Curso (string Nombre, int Duracion, Profesor Instructor) {
        this.Id = _idCount++;
        this.Nombre = Nombre;
        this.Duracion = Duracion;
        this.Instructor = Instructor;
        de = new List<Inscripcion> (capacity: 50);
    }

    public List<string> Inscriptos () {
        List<string> inscrString = new List<string> (capacity: 50);
        foreach (Inscripcion insc in this.de) {
            inscrString.Add (insc.alumno.Nombre);
        }

        return inscrString;
    }

    public int CantidadInscriptos () {
        return this.de.Count;
    }

    public void InscriptosQuery () {
        var inscriptosQuery = (from inscripcion in this.de select inscripcion.alumno.Nombre).ToList ();
        Console.WriteLine ("Inscriptos por query:");
        ImprimirAlumnos(inscriptosQuery);
    }

    public void InscriptosExt () {
        var inscriptosExt = this.de.Select (inscripcion => inscripcion.alumno.Nombre).ToList ();
        Console.WriteLine ("Inscriptos por extensión:");
        ImprimirAlumnos(inscriptosExt);
    }

    public static void ImprimirAlumnos (List<string> alumnos){
        foreach (string nombre in alumnos) {
            Console.WriteLine ($"·{nombre}");
        }
    }
}