using System;
using System.Collections.Generic;
using System.Linq;

public class Repositorio {

    public static List<Estudiante> GetEstudiantes () {
        var estudiantes = new List<Estudiante> ();
        Estudiante est = new Estudiante ("Jose", 38911869, 38648);
        estudiantes.Add (est);

        return estudiantes;
    }

    public static List<Profesor> GetProfesores () {
        var profesores = new List<Profesor> ();
        Profesor prof = new Profesor ("Romina", 12345678, "Titulo");
        profesores.Add (prof);
        return profesores;
    }

    public static List<Curso> GetCursos () {
        var cursos = new List<Curso> ();
        Curso cursonet = new Curso ("NET", 8);
        cursos.Add (cursonet);
        return cursos;
    }

    public static void ResultadosQuery (List<Examen> examenes, List<Curso> cursos) {
        var examenesGrp = (from examen in examenes group examen by examen.CursoId into idCurso select idCurso).ToList ();
        foreach (var grupo in examenesGrp) {
            var curso = (from cur in cursos where cur.Id == grupo.Key select cur).First();
            var examenesCurso = grupo.ToList();
            Console.WriteLine($"Para el curso {curso.Nombre} los resultados son: ");
            AprobadosQuery(examenesCurso, cursos);
            DesaprobadosQuery(examenesCurso, cursos);
        }
    }
    public static void ResultadosExt (List<Examen> examenes, List<Curso> cursos) {
        var examenesGrp = examenes.GroupBy(examen => examen.CursoId).Select(grupo => grupo).ToList();
        foreach (var grupo in examenesGrp) {
            var curso = cursos.Where(cur => cur.Id == grupo.Key).Select(cur => cur).First();
            var examenesCurso = grupo.ToList();
            Console.WriteLine($"Para el curso {curso.Nombre} los resultados son: ");
            AprobadosExt(examenesCurso, cursos);
            DesaprobadosExt(examenesCurso, cursos);
        }
    }
    public static void AprobadosQuery (List<Examen> examenes, List<Curso> cursos) {
        var examenesOrd = (from examen in examenes orderby examen.Nota descending where examen.Nota >= 7 select examen).ToList ();
        if (examenesOrd.Count == 0) {
            Console.WriteLine ("No hay aprobados.");
            return;
        }

        var estudiantes = GetEstudiantes ();
        Console.WriteLine ("Los aprobados son: ");
        ImprimirAlumnosExamen(examenesOrd);
    }
    public static void AprobadosExt (List<Examen> examenes, List<Curso> cursos) {
        var examenesOrd = examenes.OrderByDescending (examen => examen.Nota).Where (examen => examen.Nota >= 7).Select (examen => examen).ToList ();
        if (examenesOrd.Count == 0) {
            Console.WriteLine ("No hay aprobados.");
            return;
        }
        var estudiantes = GetEstudiantes ();
        Console.WriteLine ("Los aprobados son: ");
        ImprimirAlumnosExamen(examenesOrd);
    }
    public static void DesaprobadosQuery (List<Examen> examenes, List<Curso> cursos) {
        var examenesOrd = (from examen in examenes orderby examen.Nota where examen.Nota <= 7 select examen).ToList ();
        if (examenesOrd.Count == 0) {
            Console.WriteLine ("No hay desaprobados.");
            return;
        }
        var estudiantes = GetEstudiantes ();
        Console.WriteLine ("Los desaprobados son: ");
        ImprimirAlumnosExamen(examenesOrd);
    }
    public static void DesaprobadosExt (List<Examen> examenes, List<Curso> cursos) {
        var examenesOrd = examenes.OrderBy (examen => examen.Nota).Where (examen => examen.Nota <= 7).Select (examen => examen).ToList ();
        if (examenesOrd.Count == 0) {
            Console.WriteLine ("No hay desaprobados.");
            return;
        }
        
        Console.WriteLine ("Los desaprobados son: ");
        ImprimirAlumnosExamen(examenesOrd);
    }

    public static void ImprimirAlumnosExamen(List<Examen> examenes){
        var estudiantes = GetEstudiantes ();
        foreach (Examen ex in examenes) {
            var nombreEst = estudiantes.Where (est => est.Legajo == ex.LegajoEstudiante).Select (est => est.Nombre).First ();
            Console.WriteLine ($"·{nombreEst}: {ex.Nota}");
        }
    }
}