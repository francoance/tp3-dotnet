using System;
using System.Collections.Generic;

public class Persona {
    public string Nombre { get; set; }
    public long DNI { get; set; }

    public Persona (string nombre, long dni) {
        this.Nombre = nombre;
        this.DNI = dni;
    }
}